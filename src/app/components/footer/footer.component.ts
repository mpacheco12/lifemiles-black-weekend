import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public productsService: ProductsService) { }
  country:string;
  ngOnInit() {
    this.country = this.productsService.country;
    this.productsService.countryUpdated.subscribe((value) => {
      this.country = value;
    });
  }
  switchCountry(){
    this.productsService.switchCountry();
  }

}
