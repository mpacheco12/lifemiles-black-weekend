import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.3s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ],
})
export class DetalleComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute,public productsService:ProductsService) {
  this.allProducts =this.productsService.products;
 }
  id:string;
  image:string;
  category:string;
  product:any;
  actualPrice:string;
  discount:string;
  previousPrice:string;
  brand:string;
  allProducts:any[];
  relatedProducts:any[];
  ngOnInit() {
    this.activatedRoute.params.subscribe(params=>{
      this.id = params['id'];
      if(this.allProducts.length>0){
        this.getProduct();
      }

      this.productsService.productsUpdated.subscribe((value) => {
        this.allProducts = value;
        this.getProduct()
      });
    });

  }
  getProduct(){
    for(let i in this.allProducts){
      if(this.allProducts[i]['id']==this.id){
        this.product = this.allProducts[i];
        this.image = this.product.images[0].src;
        this.actualPrice=null;
        this.previousPrice=null;
        let attr = this.product.attributes;
        for (let j in attr) {
          let now = false;
          let prev=false;
          if (attr[j]['name'] == "Ahola LM") {
            this.actualPrice = "LM "+attr[j]['options'][0];
            now = true;
          }
          if (attr[j]['name'] == "Precio Antes LM") {
            this.previousPrice = "LM "+attr[j]['options'][0];
            prev = true;
          }
          if(!now){
            if (attr[j]['name'] == "Ahora COP + LM") {
              this.actualPrice = "$"+attr[j]['options'][0];
              now = true;
            }
          }
          if(!prev){
            if (attr[j]['name'] =="Precio Antes LM + COP") {
              this.previousPrice = "$"+attr[j]['options'][0];
              prev = true;
            }
          }
          if(attr[j]['name']=='Descuento'){
            this.discount = attr[j]['options'][0];
          }
          if(attr[j]['name']=="Marca"){
            this.brand = attr[j]['options'][0];
          }
        }
        let categories = this.product.categories;
        for (let j in categories) {
          if (categories[j]['name'] != 'Colombia' && categories[j]['name'] != 'Salvador') {
            this.category = categories[j]['name']
          }
        }
        break;
      }
    }
    let productCategory = this.product.categories[1].name;
    let ret=[];
    for(let i in this.allProducts){
      if(this.allProducts[i].id == this.product.id){
        continue;
      }
      let categories = this.allProducts[i]['categories'];
      let category = '';
      for (let j in categories) {
        if (categories[j]['name'] == productCategory) {
            ret.push(this.allProducts[i]);
        }
      }
    }
    const shuffled = ret.sort(() => .5 - Math.random());
    this.relatedProducts =shuffled.slice(0,3);
  }



}
