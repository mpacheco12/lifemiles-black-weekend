import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-terminos-y-condiciones',
  templateUrl: './terminos-y-condiciones.component.html',
  styleUrls: ['./terminos-y-condiciones.component.css']
})
export class TerminosYCondicionesComponent implements OnInit {

  constructor(public productsService: ProductsService) { }
  country:string;

  ngOnInit() {
    this.country = this.productsService.country;
    this.productsService.countryUpdated.subscribe((value) => {
      this.country = value;
    });
  }
}
