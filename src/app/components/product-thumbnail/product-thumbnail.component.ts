import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-thumbnail',
  templateUrl: './product-thumbnail.component.html',
  styleUrls: ['./product-thumbnail.component.css']
})
export class ProductThumbnailComponent implements OnInit {

  constructor(private router:Router) { }
  @Input() product:any;
  actualPrice:string;
  previousPrice:string;
  image:string;
  brand:string;
  discount:string;
  ngOnInit() {
    let attr = this.product['attributes'];
    for (let j in attr) {
      let now = false;
      let prev = false;
      if (attr[j]['name'] == "Ahola LM") {
        this.actualPrice = "LM "+ attr[j]['options'][0].split(' ').join('').replace('LM','');
        now = true;
      }
      if (attr[j]['name'] == "Precio Antes LM") {
        this.previousPrice = "LM "+ attr[j]['options'][0].split(' ').join('').replace('LM','');
        prev = true;
      }
      if(!now){
        if (attr[j]['name'] == "Ahora COP + LM") {
          this.actualPrice = "$"+attr[j]['options'][0];
          now = true;
        }
      }
      if(!prev){
        if (attr[j]['name'] =="Precio Antes LM + COP") {
          this.previousPrice = "$"+attr[j]['options'][0];
          prev = true;
        }
      }
      if(attr[j]['name']=='Descuento'){
        this.discount = attr[j]['options'][0];
      }
      if(attr[j]['name']=="Marca"){
        this.brand = attr[j]['options'][0];
      }
    }
    this.image = this.product['images'][0].src;
  }
  goToDetail(id){
    this.router.navigate(['/detalle',id]);
  }

}
