import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.3s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  products: any[] = [];
  allProducts: any[] = [];
  maxPointsValue: number;
  currentPointsFilter:number;
  randProducts:any[]=[];
  constructor(public productsService: ProductsService) {
    this.currentPointsFilter = this.productsService.currentPointsFilter;
    this.allProducts =this.productsService.products;
    this.filterProducts();
  }

  ngOnInit() {
    this.productsService.currentPointsFilterUpdated.subscribe((value)=>{
      this.currentPointsFilter = value;
      this.filterProducts();
    });
    this.productsService.productsUpdated.subscribe((value) => {
      this.allProducts = value;
      this.filterProducts();
    });
  }

  filterProducts(){
    let products = this.allProducts;
    let res = [];
    let ret =[]
    for (let i in products) {

      let attr = products[i]['attributes'];
      let nowPrice =0;
      for (let j in attr) {
        if (attr[j]['name'] == 'Ahola LM') {
          let nowPriceStr = attr[j]['options'][0].split(' ').join('').replace('LM', '').replace('millas','').replace('.','');
          if(nowPriceStr.indexOf('+')!=-1){
            nowPriceStr = nowPriceStr.split('+')[1];
          }
          if(nowPriceStr.indexOf('y')!=-1){
            nowPriceStr = nowPriceStr.split('y')[1];
          }
          nowPrice = parseInt(nowPriceStr, 10);
          break;
        }
        if (attr[j]['name'] == 'Ahora COP + LM') {
          let nowPriceStr = '';
          try {
            nowPriceStr = attr[j]['options'][0].split('y')[1].replace('LM ', '').replace(' millas','').replace('.','');;
          } catch (err) {
            nowPriceStr = attr[j]['options'][0].split('+')[1].replace('LM ', '').replace(' millas','').replace('.','');;
          }
          nowPrice = parseInt(nowPriceStr, 10);
          break;
        }

      }
      if (products[i]['featured']) {
        if(this.currentPointsFilter>=nowPrice){
          res.push(products[i]);
        }
      }else{
        if(this.currentPointsFilter>=nowPrice){
          ret.push(products[i]);
        }
      }
    }
    this.products = res;
    const shuffled = ret.sort(() => .5 - Math.random());
    this.randProducts = shuffled.slice(0,3);
  }
}
