import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';

declare var $ :any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public productsService:ProductsService) { }
  maxPointsValue:number = this.productsService.maxPointsValue;
  currentMaxPoints:number = this.productsService.currentPointsFilter;

  ngOnInit() {
    $('#ex1').slider({});
    let parent =this;
    $("#ex1").on("slide", function(slideEvt) {
      parent.productsService.setCurrentPointsFilter(slideEvt.value);
    });
    $("#ex1").on("change", function(slideEvt) {
      parent.productsService.setCurrentPointsFilter(slideEvt.value.newValue);
    });

    this.productsService.maxPointsValueUpdated.subscribe((value) => {
      this.maxPointsValue = value;
      this.currentMaxPoints = value;
       $('#ex1').slider('setAttribute', 'max', value).slider('setAttribute', 'value', value).slider('refresh');
     });

    this.productsService.currentPointsFilterUpdated.subscribe((value)=>{
      this.currentMaxPoints = value;
    });


  }





}
