import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { ProductsService } from '../../services/products.service';


@Component({
  selector: 'app-ropa-y-accesorios',
  templateUrl: './ropa-y-accesorios.component.html',
  styleUrls: ['./ropa-y-accesorios.component.css'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.3s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ],
})
export class RopaYAccesoriosComponent implements OnInit {

  currentPointsFilter: number;
  products: any[] = [];
  allProducts: any[] = [];
  constructor(public productsService: ProductsService) {
    this.currentPointsFilter = this.productsService.currentPointsFilter;
    this.allProducts = this.productsService.products;
    this.filterProducts();
  }
  ngOnInit() {
    this.productsService.currentPointsFilterUpdated.subscribe((value) => {
      this.currentPointsFilter = value;
      this.filterProducts();
    });
    this.productsService.productsUpdated.subscribe((value) => {
      this.allProducts = value;
      this.filterProducts();
    });
  }
  filterProducts() {
    let products = this.allProducts;
    let res = [];
    for (let i in products) {
      let attr = products[i]['attributes'];
      let nowPrice = 0;
      for (let j in attr) {
        if (attr[j]['name'] == 'Ahola LM') {
          let nowPriceStr = attr[j]['options'][0].split(' ').join('').replace('LM', '').replace('millas','').replace('.','');
          if(nowPriceStr.indexOf('+')!=-1){
            nowPriceStr = nowPriceStr.split('+')[1];
          }
          if(nowPriceStr.indexOf('y')!=-1){
            nowPriceStr = nowPriceStr.split('y')[1];
          }
          nowPrice = parseInt(nowPriceStr, 10);
          break;
        }
        if (attr[j]['name'] == 'Ahora COP + LM') {
          let nowPriceStr = '';
          try {
            nowPriceStr = attr[j]['options'][0].split('y')[1].replace('LM ', '').replace(' millas','').replace('.','');;
          } catch (err) {
            nowPriceStr = attr[j]['options'][0].split('+')[1].replace('LM ', '').replace(' millas','').replace('.','');;
          }
          nowPrice = parseInt(nowPriceStr, 10);
          break;
        }
      }
      let categories = products[i]['categories'];
      let category = '';
      for (let j in categories) {
        if (categories[j]['name'] == "Ropa y Accesorios") {
          if (this.currentPointsFilter >= nowPrice) {
            res.push(products[i]);
          }
          break;
        }
      }
    }
    this.products = res;
  }

}
