import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { EventEmitter } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {

  urlProducts: string = "http://makedigital.com.co/clientes/track/lifemiles/api";
  maxPointsValue: number;
  currentPointsFilter: number;
  country: string;
  products: any[] = [];
  maxPointsValueUpdated: EventEmitter<any> = new EventEmitter();
  productsUpdated: EventEmitter<any> = new EventEmitter();
  currentPointsFilterUpdated: EventEmitter<any> = new EventEmitter();
  countryUpdated: EventEmitter<any> = new EventEmitter();
  public switchCountry() {
    if (this.urlProducts[this.urlProducts.length - 1] == 'S') {
      this.urlProducts = "http://makedigital.com.co/clientes/track/lifemiles/api";
    } else {
      this.urlProducts = "http://makedigital.com.co/clientes/track/lifemiles/api?c=S";
    }
    this.getProducts();

  }
  setmaxPointsValue(value: number) {
    this.maxPointsValue = value;
    this.maxPointsValueUpdated.emit(this.maxPointsValue);
    this.setCurrentPointsFilter(value);
  }
  setProducts(products: any[]) {
    this.products = products;
    this.productsUpdated.emit(this.products);
  }
  setCountry(country: string) {
    this.country = country;
    this.countryUpdated.emit(this.country);
  }
  setCurrentPointsFilter(value: number) {
    this.currentPointsFilter = value;
    this.currentPointsFilterUpdated.emit(value);
  }
  constructor(private http: Http) {
    this.getProducts();
  }

  getProducts(callback?) {
    let headers = new Headers();
    return this.http.get(this.urlProducts, { headers }).map(resp => resp.json())
      .subscribe((data) => {
        this.setCountry(data['pais']);
        data = data['products'];
        this.setProducts(data);
        let max = 0;
        let result = []
        for (let i in data) {
          let nowPrice = 0;
          let attr = data[i]['attributes'];
          for (let j in attr) {
            if (attr[j]['name'] == 'Ahola LM') {
              let nowPriceStr = attr[j]['options'][0].split(' ').join('').replace('LM', '').replace('millas','').replace('.','');
              if(nowPriceStr.indexOf('+')!=-1){
                nowPriceStr = nowPriceStr.split('+')[1];
              }
              if(nowPriceStr.indexOf('y')!=-1){
                nowPriceStr = nowPriceStr.split('y')[1];
              }
              nowPrice = parseInt(nowPriceStr, 10);
              break;
            }
            if (attr[j]['name'] == 'Ahora COP + LM') {
              let nowPriceStr = '';
              try {
                nowPriceStr = attr[j]['options'][0].split('y')[1].replace('LM ', '').replace(' millas','').replace('.','');;
              } catch (err) {
                nowPriceStr = attr[j]['options'][0].split('+')[1].replace('LM ', '').replace(' millas','').replace('.','');;
              }
              nowPrice = parseInt(nowPriceStr, 10);
              break;
            }
          }
          if (nowPrice > max) {
            max = nowPrice;
          }
        }
        this.setmaxPointsValue(max);
      });
  }

}
