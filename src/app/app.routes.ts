import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SaludYBellezaComponent } from './components/salud-y-belleza/salud-y-belleza.component';
import { RopaYAccesoriosComponent } from './components/ropa-y-accesorios/ropa-y-accesorios.component';
import { HogarComponent } from './components/hogar/hogar.component';
import { EntretenimientoComponent } from './components/entretenimiento/entretenimiento.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { TerminosYCondicionesComponent } from './components/terminos-y-condiciones/terminos-y-condiciones.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'salud-y-belleza',component:SaludYBellezaComponent },
  { path: 'ropa-y-accesorios',component:RopaYAccesoriosComponent },
  { path: 'hogar',component:HogarComponent },
  { path: 'entretenimiento',component:EntretenimientoComponent },
  { path: 'detalle/:id',component:DetalleComponent },
  { path: 'terminos-y-condiciones', component:TerminosYCondicionesComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});
