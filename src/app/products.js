let products = {
  "products": [{
    "id": 179,
    "name": "Night Light Fawn",
    "slug": "night-light-fawn",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/night-light-fawn\/",
    "date_created": "2017-11-16T15:57:45",
    "date_modified": "2017-11-21T23:31:02",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>La l\u00e1mpara en globo de nieve - Venado es una preciosa l\u00e1mpara que tambi\u00e9n funciona como globo de nieve. Dispone de un mecanismo que emite luz de diferentes colores y hace que la nieve se mueva sin necesidad de agitarla. Es perfecta para acompa\u00f1ar a sus hijos en noches oscuras y se apaga sola despu\u00e9s de 2 horas de funcionamiento.<\/p>\n<p>Medidas: 18 x 14 x 15 cm<br \/>\nJuguete recomendado para ni\u00f1os mayores de 3 a\u00f1os<br \/>\nLas promociones no son acumulables<br \/>\nAplica condiciones y restricciones<br \/>\nPromocion disponible has agotar existencia<br \/>\nNo trae baterias incluidas<\/p>\n",
    "short_description": "",
    "sku": "C3",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [98, 101, 111, 99, 115],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 46,
      "name": "Entretenimiento",
      "slug": "entretenimiento"
    }],
    "tags": [],
    "images": [{
      "id": 121,
      "date_created": "2017-11-15T16:22:40",
      "date_modified": "2017-11-15T16:22:40",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c3.jpg",
      "name": "c3",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 7.614"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.264"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/happyeureka.com\/decoracion\/lamparas\/night-light-fawn"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["EUREKA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/179"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 178,
    "name": "Blue Kitchen",
    "slug": "blue-kitchen",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/blue-kitchen\/",
    "date_created": "2017-11-16T15:54:56",
    "date_modified": "2017-11-21T23:31:25",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>\"Desarrollar la capacidad de observaci\u00f3n y las habilidades sociales ahora es posible con la cocina azul de madera. Esta cocina es multifuncional y moderna. Tiene lavaplatos, estufa, horno, microondas y espacio para almacenamiento. Lo mejor es que te da espacio para cocinar y tiene utensilios de cocina para que puedas preparar cualquier cosa que imagines.<\/p>\n<p>Edad: 3+ a\u00f1os<br \/>\nMedidas: 30 x 55 x 75 cm<br \/>\nJuguete recomendado para ni\u00f1os mayores de 3 a\u00f1os<br \/>\nLas promociones no son acumulables<br \/>\nAplica condiciones y restricciones<br \/>\nPromocion disponible has agotar existencia\"<\/p>\n",
    "short_description": "",
    "sku": "C2",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [105, 113, 110, 103, 100],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 46,
      "name": "Entretenimiento",
      "slug": "entretenimiento"
    }],
    "tags": [],
    "images": [{
      "id": 120,
      "date_created": "2017-11-15T16:22:39",
      "date_modified": "2017-11-15T16:22:39",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c2.jpg",
      "name": "c2",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["139.900 + LM 10.000"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 9.930"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/happyeureka.com\/juguetes\/juegos-de-rol\/cocina-azul-de-madera?mfp=price[278820,659900]"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["EUREKA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/178"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 177,
    "name": "Wooden Bloks",
    "slug": "wooden-bloks",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/wooden-bloks\/",
    "date_created": "2017-11-16T15:50:57",
    "date_modified": "2017-11-21T23:31:56",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>\u00bfListo para utilizar tu imaginaci\u00f3n? Los bloques de madera cl\u00e1sicos coloridos, permiten a tus hijos construir cualquier cosa. Ayuda a descubrir la relaci\u00f3n causa-efecto y a desarrollar la habilidad de la motricidad fina.<\/p>\n<p>Incluye: 100 Piezas<br \/>\nEdad:1+ A\u00f1os<br \/>\nMedidas de la caja: 27 alto x 23 di\u00e1metro<br \/>\nLas promociones no son acumulables<br \/>\nAplica condiciones y restricciones<br \/>\nPromoci\u00f3n disponible has agotar existencia<br \/>\nJuguete recomendado para ni\u00f1os mayores de 1<\/p>\n",
    "short_description": "",
    "sku": "C1",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [96, 109, 104, 100, 117],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 46,
      "name": "Entretenimiento",
      "slug": "entretenimiento"
    }],
    "tags": [],
    "images": [{
      "id": 119,
      "date_created": "2017-11-15T16:22:37",
      "date_modified": "2017-11-15T16:22:37",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c1.jpg",
      "name": "c1",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 4.757"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 2.664"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/happyeureka.com\/juguetes\/bloques-y-juegos-de-construccion\/bloques-de-madera"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["EUREKA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/177"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 115,
    "name": "Buzo mujer REF 753009-317",
    "slug": "buzo-mujer-oxford-ref-753009-317",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/buzo-mujer-oxford-ref-753009-317\/",
    "date_created": "2017-11-15T16:17:14",
    "date_modified": "2017-11-16T16:19:55",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>Condiciones de la Promoci\u00f3n:\u00a0La promoci\u00f3n\u00a0de 30% de descuento tiene vigencia desde el  24 al 26 de noviembre de 2017. El descuento aplica sobre los productos de las referencias 031396-317; 010942-317; 753009-317; 040657-317. Aplica para tiendas propias y franquicias. Para conocer las tiendas participantes en el sitio web\u00a0www.oxfordjeans.com\/tiendas\/. PARA APLICAR AL BENEFICIO EL CLIENTE NUEVO DEBE INSCRIBIRSE EN LA BASE DE DATOS DE MODA OXFORD S.A POR MEDIO DEL FORMULARIO F\u00cdSICO y DIGITAL EN LAS TIENDAS.  El beneficio de la promoci\u00f3n aplica pagando \u00fanicamente con Millas Lifemiles o millas m\u00e1s dinero. El descuento se aplicar\u00e1 en el punto de pago. Esta promoci\u00f3n no es acumulable con otras ofertas, promociones o descuentos vigentes en la tienda.\u00a0No aplica para la compra de bonos regalos. El Producto no tendr\u00e1n cambio, salvo por garant\u00eda. La promoci\u00f3n no aplica para mercanc\u00eda en cambio. El descuento no aplica para mercanc\u00eda en promoci\u00f3n. <\/p>\n",
    "short_description": "",
    "sku": "C33",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [102, 104, 97, 92, 101],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 151,
      "date_created": "2017-11-15T16:23:16",
      "date_modified": "2017-11-15T16:23:16",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c33.jpg",
      "name": "c33",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 4.043"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 1.981"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["OXFORD"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/115"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 116,
    "name": "Jean mujer  REF 040657-317",
    "slug": "jean-mujer-oxford-ref-040657-317",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/jean-mujer-oxford-ref-040657-317\/",
    "date_created": "2017-11-15T16:17:14",
    "date_modified": "2017-11-16T16:20:24",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Condiciones de la Promoci\u00f3n:\u00a0La promoci\u00f3n\u00a0de 30% de descuento tiene vigencia desde el  24 al 26 de noviembre de 2017. El descuento aplica sobre los productos de las referencias 031396-317; 010942-317; 753009-317; 040657-317. Aplica para tiendas propias y franquicias. Para conocer las tiendas participantes en el sitio web\u00a0www.oxfordjeans.com\/tiendas\/. PARA APLICAR AL BENEFICIO EL CLIENTE NUEVO DEBE INSCRIBIRSE EN LA BASE DE DATOS DE MODA OXFORD S.A POR MEDIO DEL FORMULARIO F\u00cdSICO y DIGITAL EN LAS TIENDAS.  El beneficio de la promoci\u00f3n aplica pagando \u00fanicamente con Millas Lifemiles o millas m\u00e1s dinero. El descuento se aplicar\u00e1 en el punto de pago. Esta promoci\u00f3n no es acumulable con otras ofertas, promociones o descuentos vigentes en la tienda.\u00a0No aplica para la compra de bonos regalos. El Producto no tendr\u00e1n cambio, salvo por garant\u00eda. La promoci\u00f3n no aplica para mercanc\u00eda en cambio. El descuento no aplica para mercanc\u00eda en promoci\u00f3n. <\/p>\n",
    "short_description": "",
    "sku": "C34",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [107, 98, 117, 102, 113],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 152,
      "date_created": "2017-11-15T16:23:17",
      "date_modified": "2017-11-15T16:23:17",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c34.jpg",
      "name": "c34",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 8.090"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["LM 3.964"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["OXFORD"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/116"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 117,
    "name": "Camisa tipo polo",
    "slug": "polos",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/polos\/",
    "date_created": "2017-11-15T16:17:14",
    "date_modified": "2017-11-16T16:20:38",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Descuento exclusivo para medio de pago Millas <\/p>\n",
    "short_description": "",
    "sku": "C35",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [102, 108, 105, 107, 96],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 153,
      "date_created": "2017-11-15T16:23:19",
      "date_modified": "2017-11-15T16:23:19",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c35.jpg",
      "name": "c35",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 4.281"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 2.098"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["AMERICAN EAGLE OUTFITTERS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/117"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 105,
    "name": "Bolso L\u00ednea CESTARIA  Ref 3453",
    "slug": "bolso-linea-cestaria-ref-3453",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bolso-linea-cestaria-ref-3453\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-15T16:51:43",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Linea producida en cuero colombiano, de grano sutil y delicioso tacto.  Siluetas suaves que sigue tendencias organicas y que se adaptan a la mujer que se caracteriza por su sencillez y elegancia.  Un clasico de Boots'n Bags.<\/p>\n<p>El descuento 50% aplica para las referencias que conforman unicamente la linea Cestaria (3450, 3451, 3452, 3453 y 3454) si empre y cuando el m\u00e9todo de pago sea con millas o millas + efectivo, no aplica para pago total en efectivo, para acceder a este beneficio el socio LifeMiles deber\u00e1 presentar la tarjeta que lo acredita como aliado para poder realizar la redenci\u00f3n de las millas, en el proceso de redenci\u00f3n no aplica la acumulaci\u00f3n millas, solo se acumularan sobre el valor pagado en efectivo  como excedente en caso que el m\u00e9todo  de pago sea millas + efectivo. Existencia de 220 unidades en total a nivel nacional, disponibles segun inventario de cada tienda . Beneficio valido del 24 al 27 de Noviembre 2017. Aplica para las tiendas BOOTS \u2018N BAGS, no aplica para Outlets, no es acumulable con otras promociones ni descuento de la tarjeta VIP CLUB.<\/p>\n",
    "short_description": "",
    "sku": "C23",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [115, 109, 93, 95, 117],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 141,
      "date_created": "2017-11-15T16:23:06",
      "date_modified": "2017-11-15T16:23:06",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c23.jpg",
      "name": "c23",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["204.000 y 10.000 millas"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["6.900"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["BOOTS N BAGS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/105"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 106,
    "name": "Bolso L\u00ednea CESTARIA  Ref 3454",
    "slug": "bolso-linea-cestaria-ref-3454",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bolso-linea-cestaria-ref-3454\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-15T16:50:08",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Linea producida en cuero colombiano, de grano sutil y delicioso tacto.  Siluetas suaves que sigue tendencias organicas y que se adaptan a la mujer que se caracteriza por su sencillez y elegancia.  Un clasico de Boots'n Bags.<\/p>\n<p>El descuento 50% aplica para las referencias que conforman unicamente la linea Cestaria (3450, 3451, 3452, 3453 y 3454) si empre y cuando el m\u00e9todo de pago sea con millas o millas + efectivo, no aplica para pago total en efectivo, para acceder a este beneficio el socio LifeMiles deber\u00e1 presentar la tarjeta que lo acredita como aliado para poder realizar la redenci\u00f3n de las millas, en el proceso de redenci\u00f3n no aplica la acumulaci\u00f3n millas, solo se acumularan sobre el valor pagado en efectivo  como excedente en caso que el m\u00e9todo  de pago sea millas + efectivo. Existencia de 220 unidades en total a nivel nacional, disponibles segun inventario de cada tienda . Beneficio valido del 24 al 27 de Noviembre 2017. Aplica para las tiendas BOOTS \u2018N BAGS, no aplica para Outlets, no es acumulable con otras promociones ni descuento de la tarjeta VIP CLUB.<\/p>\n",
    "short_description": "",
    "sku": "C24",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [104, 95, 117, 98, 107],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 142,
      "date_created": "2017-11-15T16:23:07",
      "date_modified": "2017-11-15T16:23:07",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c24.jpg",
      "name": "c24",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["54.000 + LM10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["LM 4.400"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["BOOTS N BAGS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/106"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 107,
    "name": "Zapato.4170102",
    "slug": "zap-4170102",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/zap-4170102\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-16T16:10:59",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>- El 20% de descuento sobre la referencia solo aplica pagando el valor total del producto con millas LifeMiles.<br \/>\n- No acumulable con otras promociones.<br \/>\n- Valido \u00fanicamente para compras en almacenes CALZATODO.<\/p>\n",
    "short_description": "",
    "sku": "C25",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [111, 100, 108, 95, 102],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 143,
      "date_created": "2017-11-15T16:23:08",
      "date_modified": "2017-11-15T16:23:08",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c25.jpg",
      "name": "c25",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 4.281"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 2.397"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.calzatodo.com.co\/mujer\/41341-zap4170102.html"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["CALZATODO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/107"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 108,
    "name": "Sandalia M-MALBA",
    "slug": "sand-m-malba",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/sand-m-malba\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-16T16:11:09",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>- El 20% de descuento sobre la referencia solo aplica pagando el valor total del producto con millas LifeMiles.<br \/>\n- No acumulable con otras promociones.<br \/>\n- Valido \u00fanicamente para compras en almacenes CALZATODO.<\/p>\n",
    "short_description": "",
    "sku": "C26",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [99, 98, 116, 92, 107],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 144,
      "date_created": "2017-11-15T16:23:09",
      "date_modified": "2017-11-15T16:23:09",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c26.jpg",
      "name": "c26",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 7.376"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.131"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.calzatodo.com.co\/mujer\/41930-sandm-malba.html"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["CALZATODO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/108"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 109,
    "name": "Zapato HOT WHEELS 7-2 H3",
    "slug": "zap-hot-wheels-7-2-h3",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/zap-hot-wheels-7-2-h3\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-16T16:11:23",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>- El 20% de descuento sobre la referencia solo aplica pagando el valor total del producto con millas LifeMiles.<br \/>\n- No acumulable con otras promociones.<br \/>\n- Valido \u00fanicamente para compras en almacenes CALZATODO.<\/p>\n",
    "short_description": "",
    "sku": "C27",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [113, 95, 115, 101, 117],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 145,
      "date_created": "2017-11-15T16:23:10",
      "date_modified": "2017-11-15T16:23:10",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c27.jpg",
      "name": "c27",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 3.567"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 1.997"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.calzatodo.com.co\/infantil\/41298-zaphot-wheels-7-2-h3.html"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["CALZATODO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/109"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 110,
    "name": "Combo Relaxoflex Doble (una Cara)",
    "slug": "combo-relaxoflex-doble-una-cara",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/combo-relaxoflex-doble-una-cara\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-20T16:57:03",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>Colchon ortopedico suave  140*190 , uso por un solo lado  grantia  de  7 a\u00f1os incluye la base cama  140*190 en microfibra negra o cafe + protector acolchado de 140*190 + 2 almohadas siliconadas <\/p>\n",
    "short_description": "<p>Colchones Para\u00edso garantiza la disponibilidad de las promociones y mantendr\u00e1 las condiciones publicadas siempre y cuando la promoci\u00f3n se encuentre en vigencia. Las im\u00e1genes publicadas son ilustrativas y pueden distar de la realidad. Estos precios ya incluyen los descuentos ofrecidos en los puntos de venta. Aplican condiciones y restricciones, no acumulable con otras promociones vigentes en el punto de venta.<\/p>\n",
    "sku": "C28",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [102, 111, 115, 101, 103],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 146,
      "date_created": "2017-11-15T16:23:11",
      "date_modified": "2017-11-15T16:23:11",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c28.jpg",
      "name": "c28",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["2.057.000 + LM 10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["538.790 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["63%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/www.colchonesparaiso.com.co\/tienda\/paraiso\/combos\/30\/relaxoflex_std_140x190_combo"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["COLCHONES PARA\u00cdSO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/110"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 111,
    "name": "Combo Emotion  Doble (una Cara)",
    "slug": "combo-emotion-doble-una-cara",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/combo-emotion-doble-una-cara\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-20T16:56:51",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Colchon ortopedico Semi firne   140*190 , uso por un solo lado  grantia  de  7 a\u00f1os incluye la base cama  140*190 en microfibra negra o cafe + protector acolchado de 140*190 + 2 almohadas siliconadas <\/p>\n",
    "short_description": "<p>Colchones Para\u00edso garantiza la disponibilidad de las promociones y mantendr\u00e1 las condiciones publicadas siempre y cuando la promoci\u00f3n se encuentre en vigencia. Las im\u00e1genes publicadas son ilustrativas y pueden distar de la realidad. Estos precios ya incluyen los descuentos ofrecidos en los puntos de venta. Aplican condiciones y restricciones, no acumulable con otras promociones vigentes en el punto de venta.<\/p>\n",
    "sku": "C29",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [103, 101, 110, 95, 98],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 147,
      "date_created": "2017-11-15T16:23:12",
      "date_modified": "2017-11-15T16:23:12",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c29.jpg",
      "name": "c29",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["2.297.000 + LM 10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["627.590 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["63%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/www.colchonesparaiso.com.co\/tienda\/paraiso\/combos\/50\/paradise_emotion_std_140x190_combo"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["COLCHONES PARA\u00cdSO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/111"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 112,
    "name": "Combo Ortholife Plus  Doble (una Cara)",
    "slug": "combo-ortholife-plus-doble-una-cara",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/combo-ortholife-plus-doble-una-cara\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-21T23:34:41",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Colchon ortopedico Firme 140*190 , uso por un solo lado  grantia  de  7 a\u00f1os incluye la base cama  140*190 en microfibra negra o cafe + protector acolchado de 140*190 + 2 almohadas siliconadas <\/p>\n",
    "short_description": "<p>Colchones Para\u00edso garantiza la disponibilidad de las promociones y mantendr\u00e1 las condiciones publicadas siempre y cuando la promoci\u00f3n se encuentre en vigencia. Las im\u00e1genes publicadas son ilustrativas y pueden distar de la realidad. Estos precios ya incluyen los descuentos ofrecidos en los puntos de venta. Aplican condiciones y restricciones, no acumulable con otras promociones vigentes en el punto de venta.<\/p>\n",
    "sku": "C30",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [110, 100, 101, 95, 115],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 148,
      "date_created": "2017-11-15T16:23:13",
      "date_modified": "2017-11-15T16:23:13",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c30.jpg",
      "name": "c30",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["2.781.000 + LM 10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["806.670 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["63%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/www.colchonesparaiso.com.co\/tienda\/paraiso\/combos\/114\/ortholife_plus_std_140x190_combo"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["COLCHONES PARA\u00cdSO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/112"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 113,
    "name": "Camisa hombre REF 031396-317",
    "slug": "camisa-hombre-oxford-ref-031396-317",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/camisa-hombre-oxford-ref-031396-317\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-16T16:19:09",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Condiciones de la Promoci\u00f3n:\u00a0La promoci\u00f3n\u00a0de 30% de descuento tiene vigencia desde el  24 al 26 de noviembre de 2017. El descuento aplica sobre los productos de las referencias 031396-317; 010942-317; 753009-317; 040657-317. Aplica para tiendas propias y franquicias. Para conocer las tiendas participantes en el sitio web\u00a0www.oxfordjeans.com\/tiendas\/. PARA APLICAR AL BENEFICIO EL CLIENTE NUEVO DEBE INSCRIBIRSE EN LA BASE DE DATOS DE MODA OXFORD S.A POR MEDIO DEL FORMULARIO F\u00cdSICO y DIGITAL EN LAS TIENDAS.  El beneficio de la promoci\u00f3n aplica pagando \u00fanicamente con Millas Lifemiles o millas m\u00e1s dinero. El descuento se aplicar\u00e1 en el punto de pago. Esta promoci\u00f3n no es acumulable con otras ofertas, promociones o descuentos vigentes en la tienda.\u00a0No aplica para la compra de bonos regalos. El Producto no tendr\u00e1n cambio, salvo por garant\u00eda. La promoci\u00f3n no aplica para mercanc\u00eda en cambio. El descuento no aplica para mercanc\u00eda en promoci\u00f3n. <\/p>\n",
    "short_description": "",
    "sku": "C31",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [106, 117, 99, 107, 94],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 149,
      "date_created": "2017-11-15T16:23:14",
      "date_modified": "2017-11-15T16:23:14",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c31.jpg",
      "name": "c31",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 5.710"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 2.798"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["OXFORD"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/113"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 114,
    "name": "Jean hombre REF 010942-317",
    "slug": "jean-hombre-oxford-ref-010942-317",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/jean-hombre-oxford-ref-010942-317\/",
    "date_created": "2017-11-15T16:17:13",
    "date_modified": "2017-11-16T16:19:41",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Condiciones de la Promoci\u00f3n:\u00a0La promoci\u00f3n\u00a0de 30% de descuento tiene vigencia desde el  24 al 26 de noviembre de 2017. El descuento aplica sobre los productos de las referencias 031396-317; 010942-317; 753009-317; 040657-317. Aplica para tiendas propias y franquicias. Para conocer las tiendas participantes en el sitio web\u00a0www.oxfordjeans.com\/tiendas\/. PARA APLICAR AL BENEFICIO EL CLIENTE NUEVO DEBE INSCRIBIRSE EN LA BASE DE DATOS DE MODA OXFORD S.A POR MEDIO DEL FORMULARIO F\u00cdSICO y DIGITAL EN LAS TIENDAS.  El beneficio de la promoci\u00f3n aplica pagando \u00fanicamente con Millas Lifemiles o millas m\u00e1s dinero. El descuento se aplicar\u00e1 en el punto de pago. Esta promoci\u00f3n no es acumulable con otras ofertas, promociones o descuentos vigentes en la tienda.\u00a0No aplica para la compra de bonos regalos. El Producto no tendr\u00e1n cambio, salvo por garant\u00eda. La promoci\u00f3n no aplica para mercanc\u00eda en cambio. El descuento no aplica para mercanc\u00eda en promoci\u00f3n. <\/p>\n",
    "short_description": "",
    "sku": "C32",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [108, 113, 115, 98, 102],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 150,
      "date_created": "2017-11-15T16:23:15",
      "date_modified": "2017-11-15T16:23:15",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c32.jpg",
      "name": "c32",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 9.043"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.431"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["OXFORD"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/114"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 102,
    "name": "Bolso L\u00ednea CESTARIA  Ref 3450",
    "slug": "bolso-linea-cestaria-ref-3450",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bolso-linea-cestaria-ref-3450\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-22T17:08:26",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Linea producida en cuero colombiano, de grano sutil y delicioso tacto.  Siluetas suaves que sigue tendencias organicas y que se adaptan a la mujer que se caracteriza por su sencillez y elegancia.  Un clasico de Boots'n Bags.<\/p>\n<p>El descuento 50% aplica para las referencias que conforman unicamente la linea Cestaria (3450, 3451, 3452, 3453 y 3454) si empre y cuando el m\u00e9todo de pago sea con millas o millas + efectivo, no aplica para pago total en efectivo, para acceder a este beneficio el socio LifeMiles deber\u00e1 presentar la tarjeta que lo acredita como aliado para poder realizar la redenci\u00f3n de las millas, en el proceso de redenci\u00f3n no aplica la acumulaci\u00f3n millas, solo se acumularan sobre el valor pagado en efectivo  como excedente en caso que el m\u00e9todo  de pago sea millas + efectivo. Existencia de 220 unidades en total a nivel nacional, disponibles segun inventario de cada tienda . Beneficio valido del 24 al 27 de Noviembre 2017. Aplica para las tiendas BOOTS \u2018N BAGS, no aplica para Outlets, no es acumulable con otras promociones ni descuento de la tarjeta VIP CLUB.<\/p>\n",
    "short_description": "",
    "sku": "C20",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [100, 110, 113, 97, 116],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 138,
      "date_created": "2017-11-15T16:23:01",
      "date_modified": "2017-11-15T16:23:01",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c20.jpg",
      "name": "c20",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["313.000 + LM 10.000"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 8.717"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["BOOTS N BAGS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/102"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 103,
    "name": "Bolso L\u00ednea CESTARIA  Ref 3451",
    "slug": "bolso-linea-cestaria-ref-3451",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bolso-linea-cestaria-ref-3451\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-22T17:16:31",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>Linea producida en cuero colombiano, de grano sutil y delicioso tacto.  Siluetas suaves que sigue tendencias organicas y que se adaptan a la mujer que se caracteriza por su sencillez y elegancia.  Un clasico de Boots'n Bags.<\/p>\n<p>El descuento 50% aplica para las referencias que conforman unicamente la linea Cestaria (3450, 3451, 3452, 3453 y 3454) si empre y cuando el m\u00e9todo de pago sea con millas o millas + efectivo, no aplica para pago total en efectivo, para acceder a este beneficio el socio LifeMiles deber\u00e1 presentar la tarjeta que lo acredita como aliado para poder realizar la redenci\u00f3n de las millas, en el proceso de redenci\u00f3n no aplica la acumulaci\u00f3n millas, solo se acumularan sobre el valor pagado en efectivo  como excedente en caso que el m\u00e9todo  de pago sea millas + efectivo. Existencia de 220 unidades en total a nivel nacional, disponibles segun inventario de cada tienda . Beneficio valido del 24 al 27 de Noviembre 2017. Aplica para las tiendas BOOTS \u2018N BAGS, no aplica para Outlets, no es acumulable con otras promociones ni descuento de la tarjeta VIP CLUB.<\/p>\n",
    "short_description": "",
    "sku": "C21",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [111, 109, 107, 104, 114],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 139,
      "date_created": "2017-11-15T16:23:03",
      "date_modified": "2017-11-15T16:23:03",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c21.jpg",
      "name": "c21",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["292.000 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["41.000 + LM 10.000"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["BOOTS N BAGS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/103"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 104,
    "name": "Bolso L\u00ednea CESTARIA  Ref 3452",
    "slug": "bolso-linea-cestaria-ref-3452",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bolso-linea-cestaria-ref-3452\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-22T17:17:23",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Linea producida en cuero colombiano, de grano sutil y delicioso tacto.  Siluetas suaves que sigue tendencias organicas y que se adaptan a la mujer que se caracteriza por su sencillez y elegancia.  Un clasico de Boots'n Bags.<\/p>\n<p>El descuento 50% aplica para las referencias que conforman unicamente la linea Cestaria (3450, 3451, 3452, 3453 y 3454) si empre y cuando el m\u00e9todo de pago sea con millas o millas + efectivo, no aplica para pago total en efectivo, para acceder a este beneficio el socio LifeMiles deber\u00e1 presentar la tarjeta que lo acredita como aliado para poder realizar la redenci\u00f3n de las millas, en el proceso de redenci\u00f3n no aplica la acumulaci\u00f3n millas, solo se acumularan sobre el valor pagado en efectivo  como excedente en caso que el m\u00e9todo  de pago sea millas + efectivo. Existencia de 220 unidades en total a nivel nacional, disponibles segun inventario de cada tienda . Beneficio valido del 24 al 27 de Noviembre 2017. Aplica para las tiendas BOOTS \u2018N BAGS, no aplica para Outlets, no es acumulable con otras promociones ni descuento de la tarjeta VIP CLUB.<\/p>\n",
    "short_description": "",
    "sku": "C22",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [114, 112, 117, 116, 108],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 140,
      "date_created": "2017-11-15T16:23:04",
      "date_modified": "2017-11-15T16:23:04",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c22.jpg",
      "name": "c22",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["119.000 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["LM 5.483"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["BOOTS N BAGS"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/104"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 95,
    "name": "Aud\u00edfonos Ink\u00b4d BT",
    "slug": "audifonos-inkd-bt",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/audifonos-inkd-bt\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-15T17:15:51",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Ultra ligeros alrededor del cuello.<br \/>\nBluetooth\u00ae Wireless con 8-horas de bater\u00eda recargable.<br \/>\nHaz llamadas y controla tu m\u00fasica con el micr\u00f3fono y bot\u00f3n integrados en el cable.<br \/>\nDisponible en 5 colores, oferta v\u00e1lida hasta agotar existencias.<\/p>\n",
    "short_description": "",
    "sku": "C13",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [112, 117, 103, 108, 99],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 131,
      "date_created": "2017-11-15T16:22:52",
      "date_modified": "2017-11-15T16:22:52",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c13.jpg",
      "name": "c13",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 7.571"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.240"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendadoppler.com.co\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["DOOPLER"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/95"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 96,
    "name": "Bicicleta Giant Defy Advanced pro 2 2016",
    "slug": "bicicleta-giant-defy-advanced-pro-2-2016",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/bicicleta-giant-defy-advanced-pro-2-2016\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-22T17:09:27",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>Modelo 2016.<br \/>\nMarco, tenedor y tubo de sill\u00edn en carbono.<br \/>\nGrupo Shimano ultegra 11 velocidades.<br \/>\nFrenos de disco hidr\u00e1ulicos Shimano RS685.<br \/>\nRuedas Giant SL1 disc de aluminio.<br \/>\nDisponible en 1 color, 3und talla XS, 5und talla S, 5und talla M \u00fanicamente en nuestro PV Mayorca, s\u00f3lo se factura y entrega en este punto de venta (no tiene posibilidad de traslado).<\/p>\n",
    "short_description": "",
    "sku": "C14",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [117, 111, 113, 116, 104],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 46,
      "name": "Entretenimiento",
      "slug": "entretenimiento"
    }],
    "tags": [],
    "images": [{
      "id": 132,
      "date_created": "2017-11-15T16:22:53",
      "date_modified": "2017-11-15T16:22:53",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c14.jpg",
      "name": "c14",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["9.780.000 + LM10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["6.693.000 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendadoppler.com.co\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["DOOPLER"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/96"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 97,
    "name": "Colch\u00f3n original 25 160*190 c6",
    "slug": "colchon-original-25-160190-c6",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/colchon-original-25-160190-c6\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-16T16:03:56",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "",
    "short_description": "",
    "sku": "C15",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [107, 92, 103, 94, 114],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 133,
      "date_created": "2017-11-15T16:22:54",
      "date_modified": "2017-11-15T16:22:54",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c15.jpg",
      "name": "c15",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["9.029.000 + LM 10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["6.137.300 +LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/co.tempur.com\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["TEMPUR"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/97"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 98,
    "name": "Colch\u00f3n hybrid 25 200*200",
    "slug": "colchon-hybrid-25-200200",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/colchon-hybrid-25-200200\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-16T16:04:05",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "",
    "short_description": "",
    "sku": "C16",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [94, 114, 108, 99, 100],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 134,
      "date_created": "2017-11-15T16:22:56",
      "date_modified": "2017-11-15T16:22:56",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c16.jpg",
      "name": "c16",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["10.059.000 + LM10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["7.915.200 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["20%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/co.tempur.com\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["TEMPUR"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/98"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 99,
    "name": "Decorado titatium 25x50 cm",
    "slug": "decorado-titatium-25x50-cm",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/decorado-titatium-25x50-cm\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-21T23:33:08",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Hasta agotar Inventario<\/p>\n",
    "short_description": "",
    "sku": "C17",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [105, 106, 102, 97, 100],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 135,
      "date_created": "2017-11-15T16:22:57",
      "date_modified": "2017-11-15T16:22:57",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c17.jpg",
      "name": "c17",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["1.900"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["665"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["CORONA LISTO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/99"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 100,
    "name": "Decoradolana beige 25x50 cm",
    "slug": "decoradolana-beige-25x50-cm",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/decoradolana-beige-25x50-cm\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-21T23:33:27",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>Hasta agotar Inventario<\/p>\n",
    "short_description": "",
    "sku": "C18",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [97, 98, 110, 112, 113],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 136,
      "date_created": "2017-11-15T16:22:58",
      "date_modified": "2017-11-15T16:22:58",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c18.jpg",
      "name": "c18",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["1.948"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["682"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["CORONA LISTO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/100"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 101,
    "name": "Decorado Lita negro oro 25x50 cm",
    "slug": "decorado-lita-negro-oro-25x50-cm",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/decorado-lita-negro-oro-25x50-cm\/",
    "date_created": "2017-11-15T16:17:12",
    "date_modified": "2017-11-21T23:32:44",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Hasta agotar Inventario<\/p>\n",
    "short_description": "",
    "sku": "C19",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [103, 117, 104, 98, 92],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 19,
      "name": "Hogar",
      "slug": "hogar"
    }],
    "tags": [],
    "images": [{
      "id": 137,
      "date_created": "2017-11-15T16:23:00",
      "date_modified": "2017-11-15T16:23:00",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c19.jpg",
      "name": "c19",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["4.757"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["50%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["1.665"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["CORONA LISTO"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/101"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 86,
    "name": "Limpieza dental completa (profilaxis+detartraje)",
    "slug": "limpieza-dental-completa-profilaxisdetartraje",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/limpieza-dental-completa-profilaxisdetartraje\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-15T18:17:48",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Aplican condiciones y restricciones.<\/p>\n",
    "short_description": "",
    "sku": "C4",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [109, 104, 97, 107, 100],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 17,
      "name": "Salud y Belleza",
      "slug": "salud-y-belleza"
    }],
    "tags": [],
    "images": [{
      "id": 122,
      "date_created": "2017-11-15T16:22:41",
      "date_modified": "2017-11-15T16:22:41",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c4.jpg",
      "name": "c4",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["6.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["66%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["2.000"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/www.1485dentalspa.com"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["DENTAL SPA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/86"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 87,
    "name": "Blanqueamiento dental laser con luz LED fria",
    "slug": "blanqueamiento-dental-laser-con-luz-led-friac5",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/blanqueamiento-dental-laser-con-luz-led-friac5\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-15T18:15:41",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Incluye hasta 5 sesiones para realizarlas en el periodo de un mes. Aplican condiciones y restricciones.<\/p>\n",
    "short_description": "",
    "sku": "C5",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [110, 101, 105, 109, 116],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 17,
      "name": "Salud y Belleza",
      "slug": "salud-y-belleza"
    }],
    "tags": [],
    "images": [{
      "id": 123,
      "date_created": "2017-11-15T16:22:42",
      "date_modified": "2017-11-15T16:22:42",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c5.jpg",
      "name": "c5",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 35.000"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["71%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["http:\/\/www.1485dentalspa.com"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["DENTAL SPA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/87"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 88,
    "name": "Ciclo de desintoxicaci\u00f3n (3 sesiones)",
    "slug": "ciclo-de-desintoxicacion-3-sesiones",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/ciclo-de-desintoxicacion-3-sesiones\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-22T16:11:18",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Tratamiento que permite disminuir problemas de salud causados por el estr\u00e9s, insomnio, permite eliminar radicales libres y rejuvenecer el interior de las c\u00e9lulas, aplica solo para compras en los d\u00edas autorizados para BLACK WEEKEND, m\u00e1ximo 2 tratamientos x persona, vigencia para uso diciembre 28 de 2017, aplica solo para SERGIO RADA BOGOTA<\/p>\n",
    "short_description": "",
    "sku": "C6",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [105, 114, 111, 107, 101],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 17,
      "name": "Salud y Belleza",
      "slug": "salud-y-belleza"
    }],
    "tags": [],
    "images": [{
      "id": 124,
      "date_created": "2017-11-15T16:22:43",
      "date_modified": "2017-11-15T16:22:43",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c6.jpg",
      "name": "c6",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 1,
      "name": "Precio Antes LM + COP",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["1.230.000 + LM 10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["708.000 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.sergiorada.com.co"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["SERGIO RADA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/88"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 89,
    "name": "Limpieza facial profunda",
    "slug": "limpieza-facial-profunda",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/limpieza-facial-profunda\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-22T16:02:23",
    "type": "simple",
    "status": "publish",
    "featured": true,
    "catalog_visibility": "visible",
    "description": "<p>La limpieza facial es fundamental para mantener una piel sana y bella ya que permite eliminar las impurezas que d\u00eda tras d\u00eda obtenemos por factores como la contaminaci\u00f3n ambiental o el maquillaje, adem\u00e1s proporciona oxigenaci\u00f3n permitiendo mayor luminosidad en la piel y por ultimo una relajaci\u00f3n de los m\u00fasculos del rostro. aplica solo para compras en los d\u00edas autorizados para BLACK WEEKEND, m\u00e1ximo 2 tratamientos x persona, vigencia para uso diciembre 28 de 2017, aplica solo para SERGIO RADA BOGOTA<\/p>\n",
    "short_description": "",
    "sku": "C7",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [106, 111, 114, 96, 92],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 17,
      "name": "Salud y Belleza",
      "slug": "salud-y-belleza"
    }],
    "tags": [],
    "images": [{
      "id": 125,
      "date_created": "2017-11-15T16:22:45",
      "date_modified": "2017-11-15T16:22:45",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c7.jpg",
      "name": "c7",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 6.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["66%"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["LM 2.000"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.sergiorada.com.co"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["SERGIO RADA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/89"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 90,
    "name": "Depilaci\u00f3n laser bikini + axila",
    "slug": "depilacion-laser-bikini-axila",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/depilacion-laser-bikini-axila\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-22T16:20:16",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Consiste en la eliminaci\u00f3n progresiva del vello, gracias a la respuesta foto-termo- selectiva de la melanina del pelo al rayo l\u00e1ser, mejora el tono de la piel ya que activa la elastina y col\u00e1geno. aplica solo para compras en los d\u00edas autorizados para BLACK WEEKEND, m\u00e1ximo 2 tratamientos x persona, vigencia para uso diciembre 28 de 2017, aplica solo para SERGIO RADA BOGOTA<\/p>\n",
    "short_description": "",
    "sku": "C8",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [96, 92, 117, 99, 98],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 17,
      "name": "Salud y Belleza",
      "slug": "salud-y-belleza"
    }],
    "tags": [],
    "images": [{
      "id": 126,
      "date_created": "2017-11-15T16:22:46",
      "date_modified": "2017-11-15T16:22:46",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c8.jpg",
      "name": "c8",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["459.000 + LM10.000"]
    }, {
      "id": 4,
      "name": "Ahora COP + LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["201.750 + LM 10.000"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["25%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["www.sergiorada.com.co"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["SERGIO RADA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/90"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 91,
    "name": "Sandalia WRN 444",
    "slug": "sandalia-wrn-444",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/sandalia-wrn-444\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-16T16:03:15",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>SANDALIA 00% CUERO , APLICA TODOS LOS COLORES , SUJETO A DISPONIBILIDAD EN TIENDA<\/p>\n",
    "short_description": "",
    "sku": "C9",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [98, 104, 92, 96, 107],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 127,
      "date_created": "2017-11-15T16:22:47",
      "date_modified": "2017-11-15T16:22:47",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c9.jpg",
      "name": "c9",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 8.666"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.247"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendasjosh.com\/shop\/wrn444-2\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["JOSH"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/91"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 92,
    "name": "Sandalia ALZ386CUE",
    "slug": "sandalia-alz386cue",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/sandalia-alz386cue\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-16T16:03:35",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>SANDALIA 00% CUERO , APLICA TODOS LOS COLORES , SUJETO A DISPONIBILIDAD EN TIENDA<\/p>\n",
    "short_description": "",
    "sku": "C10",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [95, 99, 110, 97, 108],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 128,
      "date_created": "2017-11-15T16:22:48",
      "date_modified": "2017-11-15T16:22:48",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c10.jpg",
      "name": "c10",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 9.000"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.410"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendasjosh.com\/shop\/alz386cue-2\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["JOSH"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/92"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 93,
    "name": "Deportivo kokomo 01",
    "slug": "deportivo-kokomo-01",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/deportivo-kokomo-01\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-16T16:03:47",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>ZAPATO DEPORTIVO 1 00% CUERO , APLICA TODOS LOS COLORES , SUJETO A DISPONIBILIDAD EN TIENDA<\/p>\n",
    "short_description": "",
    "sku": "C11",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [98, 113, 104, 108, 110],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 129,
      "date_created": "2017-11-15T16:22:49",
      "date_modified": "2017-11-15T16:22:49",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c11.jpg",
      "name": "c11",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 10.000"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 4.900"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["30%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendasjosh.com\/shop\/kokomo01-2\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["JOSH"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/93"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }, {
    "id": 94,
    "name": "Morral Night Track",
    "slug": "morral-night-track",
    "permalink": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/producto\/morral-night-track\/",
    "date_created": "2017-11-15T16:17:11",
    "date_modified": "2017-11-15T17:17:31",
    "type": "simple",
    "status": "publish",
    "featured": false,
    "catalog_visibility": "visible",
    "description": "<p>Tama\u00f1o: 45 x 31 x 19 cm<br \/>\nCapacidad: 19.25 liters<br \/>\n1 compartimento principal grande<br \/>\nCorreas de hombro acolchadas<br \/>\nFunda interna acolchada para computadora port\u00e1til de 15 \"\"<br \/>\nDisponible en 3 colores, oferta v\u00e1lida hasta agotar existencias.<\/p>\n",
    "short_description": "",
    "sku": "C12",
    "price": "",
    "regular_price": "",
    "sale_price": "",
    "date_on_sale_from": "",
    "date_on_sale_to": "",
    "price_html": "",
    "on_sale": false,
    "purchasable": false,
    "total_sales": 0,
    "virtual": false,
    "downloadable": false,
    "downloads": [],
    "download_limit": -1,
    "download_expiry": -1,
    "download_type": "standard",
    "external_url": "",
    "button_text": "",
    "tax_status": "taxable",
    "tax_class": "",
    "manage_stock": false,
    "stock_quantity": null,
    "in_stock": true,
    "backorders": "no",
    "backorders_allowed": false,
    "backordered": false,
    "sold_individually": false,
    "weight": "",
    "dimensions": {
      "length": "",
      "width": "",
      "height": ""
    },
    "shipping_required": true,
    "shipping_taxable": true,
    "shipping_class": "",
    "shipping_class_id": 0,
    "reviews_allowed": true,
    "average_rating": "0.00",
    "rating_count": 0,
    "related_ids": [99, 97, 112, 117, 103],
    "upsell_ids": [],
    "cross_sell_ids": [],
    "parent_id": 0,
    "purchase_note": "",
    "categories": [{
      "id": 15,
      "name": "Colombia",
      "slug": "colombia"
    }, {
      "id": 18,
      "name": "Ropa y Accesorios",
      "slug": "ropa-y-accesorios"
    }],
    "tags": [],
    "images": [{
      "id": 130,
      "date_created": "2017-11-15T16:22:51",
      "date_modified": "2017-11-15T16:22:51",
      "src": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-content\/uploads\/2017\/11\/c12.jpg",
      "name": "c12",
      "alt": "",
      "position": 0
    }],
    "attributes": [{
      "id": 2,
      "name": "Precio Antes LM",
      "position": 0,
      "visible": true,
      "variation": false,
      "options": ["LM 6.810"]
    }, {
      "id": 5,
      "name": "Ahola LM",
      "position": 1,
      "visible": true,
      "variation": false,
      "options": ["LM 2.680"]
    }, {
      "id": 3,
      "name": "Descuento",
      "position": 2,
      "visible": true,
      "variation": false,
      "options": ["40%"]
    }, {
      "id": 6,
      "name": "URL",
      "position": 3,
      "visible": true,
      "variation": false,
      "options": ["https:\/\/www.tiendadoppler.com.co\/"]
    }, {
      "id": 7,
      "name": "Marca",
      "position": 4,
      "visible": true,
      "variation": false,
      "options": ["EUREKA"]
    }],
    "default_attributes": [],
    "variations": [],
    "grouped_products": [],
    "menu_order": 0,
    "_links": {
      "self": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products\/94"
      }],
      "collection": [{
        "href": "http:\/\/makedigital.com.co\/clientes\/track\/lifemiles\/administrator\/wp-json\/wc\/v1\/products"
      }]
    }
  }],
  "pais": "Colombia"
}
