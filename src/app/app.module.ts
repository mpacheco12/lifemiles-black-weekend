import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { SaludYBellezaComponent } from './components/salud-y-belleza/salud-y-belleza.component';
import { RopaYAccesoriosComponent } from './components/ropa-y-accesorios/ropa-y-accesorios.component';
import { HogarComponent } from './components/hogar/hogar.component';
import { EntretenimientoComponent } from './components/entretenimiento/entretenimiento.component';

import { ProductsService } from './services/products.service';
import { ProductThumbnailComponent } from './components/product-thumbnail/product-thumbnail.component';
import { LoadingComponent } from './components/loading/loading.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { TerminosYCondicionesComponent } from './components/terminos-y-condiciones/terminos-y-condiciones.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    SaludYBellezaComponent,
    RopaYAccesoriosComponent,
    HogarComponent,
    EntretenimientoComponent,
    ProductThumbnailComponent,
    LoadingComponent,
    DetalleComponent,
    TerminosYCondicionesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    APP_ROUTING
  ],
  providers: [ProductsService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
